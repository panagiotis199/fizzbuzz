const ERRORS = {
    ELEMENT_NOT_FOUND: 'Rendering element not found.',
    INVALID_TYPE_OF_RANGE: 'Range should be an integer.',
    UNKNOWN: 'Something went wrong.',
};

export class FizzBuzz {
    /**
     * @param {string} elSelector
     */
    constructor(elSelector = '') {
        /** @type {number} Default starting range. */
        this.range = 100;

        /** @type {string} class name for the html div blocks. */
        this._fizzBuzzItemClassName = 'fizz-buzz-item';

        /** @type {Array} we will also store the values in an array except from HTML Elements.*/
        this._result = [];

        try {
            this.renderTo(elSelector);
        } catch (error) {
            this.handleError(error);
        }
    }

    /**
     * start the game.
     *
     * @return {FizzBuzz}
     */
    play() {
        try {
            let resultHtmlElements = this.parseValuesAndCreateDomElements();
            this.render(resultHtmlElements);
        } catch (error) {
            this.handleError(error);
        }

        return this;
    }

    /**
     * Parse the dataset and return the Node elements to be appended
     * to the app container.
     *
     * @return {DocumentFragment}
     */
    parseValuesAndCreateDomElements() {
        let value = 0;
        // Create document fragment to store the HTML elements.
        // and then append all of them to the DOM.
        let elementFragment = document.createDocumentFragment();

        for (let i = 1; i <= this.range; i++) {
            value = this.getFizzOrBuzz(i);
            this._result.push(value);
            elementFragment.appendChild(this.createDiv(value));
        }

        return elementFragment;
    }

    /**
     * @param {string} value
     *
     * @return {HTMLDivElement}
     */
    createDiv(value) {
        let div = document.createElement('div');

        // Omit Multiple arguments for add() because IE does not support it.
        div.classList.add(value.toString().toLowerCase());
        div.classList.add(this._fizzBuzzItemClassName);
        div.textContent = value;

        return div;
    }

    /**
     * Get "Fizz", "Buzz" or "FIZZBUZZ" depending of the given value.
     *
     * @param {int} value
     * @return {string}
     */
    getFizzOrBuzz(value) {
        let val = parseInt(value);

        if (this.valueIsFizzBuzz(val)) {
            return 'FIZZBUZZ';
        }

        if (this.valueIsFizz(val)) {
            return 'Fizz';
        }

        if (this.valueIsBuzz(val)) {
            return 'Buzz';
        }

        return value.toString();
    }

    /**
     * @param {int} val
     *
     * @return {boolean}
     */
    valueIsFizzBuzz(val) {
        return (this.valueIsFizz(val) && this.valueIsBuzz(val));
    }

    /**
     * @param {int} val
     *
     * @return {boolean}
     */
    valueIsFizz(val) {
        return (val % 3 === 0);
    }

    /**
     * @param {int} val
     *
     * @return {boolean}
     */
    valueIsBuzz(val) {
        return (val % 5 === 0);
    }

    /**
     * Render content to the HTML container of the app.
     *
     * @param {Node | string } value
     *
     * @return {FizzBuzz}
     */
    render(value) {
        let content = value;

        if (!(value instanceof Node)) {
            content = document.createTextNode(value);
        }

        this.el.appendChild(content);

        return this;
    }

    /**
     * Set the parent element to render the application.
     *
     * @param {string} elSelector
     *
     * @return {FizzBuzz}
     */
    renderTo(elSelector) {
        let el = document.querySelector(elSelector);

        if (el === null) {
            throw new Error(ERRORS.ELEMENT_NOT_FOUND);
        }

        this.el = el;

        return this;
    }

    /**
     * Set a new range.
     *
     * @param {int} newRange
     *
     * @return {FizzBuzz}
     */
    withRange(newRange) {
        if (!Number.isInteger(newRange)) {
            throw new Error(ERRORS.INVALID_TYPE_OF_RANGE);
        }

        this.range = newRange;

        return this;
    }

    /**
     * Log the error to the console and leave a message to
     * the client that something went wrong.
     *
     * @param {Error} error
     *
     * @return {FizzBuzz}
     */
    handleError(error) {
        if (!(error instanceof Error)) {
            console.error(ERRORS.UNKNOWN);
            this.render(ERRORS.UNKNOWN);
            return this;
        }

        this.render(ERRORS.UNKNOWN);
        console.error(error.message);

        return this;
    }
}
