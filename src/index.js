import 'babel-polyfill';

import './styles/styles.scss';

import {FizzBuzz} from './modules/FizzBuzz';

const fizzBuzz = new FizzBuzz('#game');

fizzBuzz.withRange(100).play();
