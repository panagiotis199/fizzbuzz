# FizzBuzz Code Exercise. 

> FizzBuzz game implementation in JavaScript

##### Online Preview at: [http://test.pmpak.com/avaloq/fizz-buzz/](http://test.pmpak.com/avaloq/fizz-buzz/)

## Summary
This application was build using ES6 syntax, webpack 4 as a build tool and SCSS as the style language.

## Local setup

open console, navigate to project's root path and run the following command:
```bash
npm install
```

After npm finishes installing the packages you can run the following commands:

npm start
>Starts the development server.

npm run build
>Bundles the app into static files for production.
       
## Additional libraries and plugins that were used
* [babel-polyfill](https://babeljs.io/docs/usage/polyfill/)

## Browsers Tested
* Chrome 67.0.3396.99
* Firefox 61.0.1
* Edge 42.17134.1.0
* Internet Explorer 11